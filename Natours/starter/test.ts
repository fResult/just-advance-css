interface Approvable {
  approve(approver: string): void
}

interface Printable {
  print(): void
}

class Invoice implements Approvable, Printable {
  print(): void {
    console.log('Invoice is printed')
  }
  approve(approver: string): void {
    console.log('Invoice is approved by', approver)
  }
}

class Receipt implements Approvable {
  approve(approver: string): void {
    console.log('Receipt is approved by', approver)
  }
}

function confirm(approvableDoc: Approvable) {
  approvableDoc.approve('Korn')
  approvableDoc instanceof Invoice ? approvableDoc.print() : null
}

confirm(new Invoice())
confirm(new Receipt())
